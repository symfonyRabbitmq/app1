<?php

namespace App\MessageHandler;

use App\Services\MailerService;
use App\Messages\UserNotificationMessage;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class UserNotifcationHandler implements MessageHandlerInterface
{
    public function __construct(
        MailerService $mailerService
    )
    {
        $this->service = $mailerService;
    }

    public function __invoke(UserNotificationMessage $message)
    {
        $this->service->sendMail(
            "romeokamgo@gmail.com",
            "Emailtemplate/Notification.html.twig",
            "test rabbitmq",
            []
        );
    }
}
