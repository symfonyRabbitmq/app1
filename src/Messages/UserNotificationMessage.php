<?php

namespace App\Messages;

class UserNotificationMessage
{

    private $msg_type;

    public function __construct()
    {
        $this->msg_type = "user_access_token";
    }


    /**
     * Get the value of msg_type
     */
    public function getMsgType()
    {
        return $this->msg_type;
    }

    /**
     * Set the value of msg_type
     */
    public function setMsgType($msg_type): self
    {
        $this->msg_type = $msg_type;

        return $this;
    }
}
