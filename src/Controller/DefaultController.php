<?php

namespace App\Controller;

use App\Messages\UserNotificationMessage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Messenger\MessageBusInterface;


class DefaultController extends AbstractController
{

    public function __construct(
        MessageBusInterface $bus
    )
    {
        $this->bus = $bus;       
    }
    
    #[Route('/', name:"index", methods:'GET')]
    public function index(): Response
    {
        $notify = new UserNotificationMessage();
        $this->bus->dispatch($notify);
        
        return new Response(
            '<html><body>Microservie 1 </body></html>'
        );
    }
}
